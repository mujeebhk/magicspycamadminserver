var serverURL = "http://localhost:3080";
var superAdminConsole = "/userConsole";
var adminConsole = "/policeConsole";
var boomerangServer = "/magicSpyCamServer";

//var superAdminConsole = "/SuperUserAdminConsoleTest42";
//var adminConsole = "/adminconsoleTest42";
//var boomerangServer = "/boomerangTest42";

//var serverURL = "http://www.boomerangcastadmin.com";
var getOptionListURL = serverURL + boomerangServer +"/api/server/eventservice/GetPoll/pollId/";
//var redirectToSuDashURL = serverURL + "/adminconsole/dashboard.html#/sudash";
var redirectToReviewrDashURL = serverURL + adminConsole + "/dashboard.html#/reviewerDash/reviewerId/";

var redirectToEventDetailsURL = serverURL + adminConsole + "/dashboard.html#/CurrentEventDetail/eventId/";
var redirectToSponsorDetailsURL  = serverURL + adminConsole + "/dashboard.html#/advertiserDetails/advertiserId/";
var redirectToAdminDetailURL = serverURL + adminConsole + "/dashboard.html#/admindetail/";

var redirectToSuperAdminDetailURL = serverURL + adminConsole + "/dashboard.html#/superadmins/";

var redirectToOrgDetailURL = serverURL + adminConsole + "/dashboard.html#/orgs/";
var redirectToPollDetailURL = serverURL + adminConsole + "/dashboard.html#/polldetails/";
var redirectToAdSellURL = serverURL + adminConsole + "/dashboard.html#/SellAd/";
var redirectToSuDashURL = serverURL + superAdminConsole +"/dashboard.html#/sudash";

var redirectToReviewerLoginURL = serverURL + adminConsole + "/AdminLogin.html#/AdminLogin";
var redirectToSellAdURL = serverURL + adminConsole + "/dashboard.html#/SellAd/orgName/";

var redirectToUpcomingEventDetailsURL = serverURL + adminConsole + "/dashboard.html#/UpcomingEventDetail/eventId/";
var redirectToPreviousEventDetailsURL = serverURL + adminConsole + "/dashboard.html#/PreviousEventDetail/eventId/";
var redirectToUpdateEventURL = serverURL + adminConsole + "/dashboard.html#/updateEvent/eventName/";
var redirectToCurrentPollCreateURL = serverURL + adminConsole + "/dashboard.html#/CurrentPollCreate/orgName/";
var redirectToUpcomingPollCreateURL = serverURL + adminConsole + "/dashboard.html#/UpcomingPollCreate/orgName/";
var redirectToUpcomingPollDetailURL = serverURL + adminConsole + "/dashboard.html#/UpcomingPolldetails/";