app
		.controller(
				'BreadCrumbCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http,$routeParams) {

							console.log('Inside BreadCrumbCtrl');
							
							//currentEvents
							//UpcomingEvents
							//PreviousEvents
							//OrgAdvertisers
							
							//sessionStorage.orgId= $routeParams.orgId;
							console.log(sessionStorage.orgId);
							var orgId = sessionStorage.orgId;
							console.log(orgId);
							$routeParams.orgId=orgId;
							
							$http
							.get(
									serverURL + boomerangServer+"/api/server/organization/getOrgById/"+ orgId + "?fetchSummary=true",
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {
										
										console.log(data);
										
										$scope.currentOrg=data;
										
										
									})
							.error(
									function(data, status, headers,
											config) {
										if(status == 401){
											checkStatus();
										}
										else{

										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});
										}

									});
							
						
							 
							
								
								
								
								
							
							
							
							$http
									.get(
											serverURL + boomerangServer+"/api/server/eventservice/listCurrentEventsForOrg/orgId/"+ orgId,
											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {
												
												
													$scope.currentEvents=data;
												
												
												 
												//$scope.currentEvents = data;

											})
									.error(
											function(data, status, headers,
													config) {

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													type : 'error'
												});

											});
							
							
							
							
							
							$scope.endEventByObject = function(event) {
								console.log('Inside endEvent function');
								
								/*window.confirm("Are You Sure To Deleting Event");
								var data =

								{
									'eventName' : $scope.event.eventName,
									'orgName' : $scope.event.orgName

								}; 

								//console.log(data);
								var r = confirm("Press a button");
								 
								 
								if (confirm == true) {
									x = "You pressed OK!";
								}*/
								
								
								 
								  
								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/eventservice/endEvent/eventName/"+event.eventName+"/orgName/"+event.orgName,
												
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {
													window.location.reload();
													var n = noty({
														text : 'Event Ended  successfully',
														layout : 'topRight',
														type : 'success'
													});
													//window.location = redirectToOrgDashURL+$scope.event.orgId;
													//window.location = redirectToEventDetailsURL+"eventName/"+$scope.event.eventName+"/eventId/"+$scope.event.eventId+"/orgName/"+$scope.event.orgName;
													
												})

										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer:'true',type : 'error'
													});

												});

							}
							
							
							
							
							
							
							
							
							$http
							.get(
									serverURL + boomerangServer+"/api/server/eventservice/listUpcomingEventsForOrg/orgId/"+ orgId,
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {
										
										$scope.upcomingEvents=data;
										
										
										//$scope.currentEvents = data;

									})
							.error(
									function(data, status, headers,
											config) {

										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});

									});
							
							
							
							$http
							.get(
									serverURL + boomerangServer+"/api/server/eventservice/listPreviousEventsForOrg/orgId/"+ orgId,
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {
										
										$scope.previousEvents=data;
										
										
										//$scope.currentEvents = data;

									})
							.error(
									function(data, status, headers,
											config) {

										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});

									});
							
							
							$http
							.get(
									serverURL + boomerangServer+"/api/server/advertiserservice/listAdvertisersForOrg/orgId/"+ orgId + '?fetchSummary=true',
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {
										
										$scope.advertisers=data;
										
										
										
										//$scope.currentEvents = data;

									})
							.error(
									function(data, status, headers,
											config) {

										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});

									});
					
							
							
						} ]);
