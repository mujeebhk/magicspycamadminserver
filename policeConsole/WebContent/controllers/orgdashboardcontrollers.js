app.controller('ForgotPasswordCtrl', function($scope, $modal, $log) {

	$scope.fileName = "";

	$scope.open = function(size) {

		var modalInstance = $modal.open({
			templateUrl : 'angularjs.html',
			controller : 'ModalInstanceCtrl1',
			size : size,
			resolve : {
				fileName : function() {
					return $scope.fileName;
				}
			}
		});

		modalInstance.result.then(function(fileName) {

		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
});

app.controller('ModalInstanceCtrl1', function($scope, $modalInstance, fileName) {

	//$scope.fileName = fileName;

	/*$scope.ok = function() {
		$scope.logoPath = $scope.fileName;
		$modalInstance.close($scope.fileName);
	};*/

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
});


app
.controller(
		'ChallanCtrl',
		[
				'$scope',
				'$http',
				'$routeParams',
				function($scope, $http, $routeParams) {
					$scope.reportId = $routeParams.reportId;

					console.log('Inside ChallanCtrl');
					
					var names = ["Ramakrishna S","Suvendu T", "Srilatha A","Subramanian Swamy","Nadeem Sharief"];
					var x = Math.floor((Math.random() * 5) + 1);
				    
				    var challanName = names[x-1]; 
					
					$http
					.get(
							serverURL + boomerangServer+"/api/server/reviewerManagerService/getChallan/"+ $routeParams.reportId,
							{
								headers : {
									'Content-Type' : 'application/json; charset=UTF-8'
								}
							})
					.success(
							function(data, status, headers,
									config) {
								
								//$scope.currentOrg=data;
								
								//$routeParams.orgName=$scope.currentOrg.orgName;
								
								$scope.challanData=data;
								$scope.name = challanName;

							})
					.error(
							function(data, status, headers,
									config) {
								if(status == 401){
									checkStatus();
								}
								else{

								var n = noty({
									text : 'Error: '
											+ headers('BoomerangError'),
									layout : 'topRight',
									type : 'error'
								});
								}

							});

					
					
				}]);

app
.controller(
		'CreateUserCtrl',
		[
				'$scope',
				'$http',
				'$routeParams',
				function($scope, $http, $routeParams) {
					$scope.username = $routeParams.username;

					console.log('Inside CreateUserCtrl');
					
					
					
				

					$scope.CreateUser = function() {
						
						
						if ($scope.username == null) {
							var n = noty({
								text : 'Error: User Name needs to be entered',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;
						}
						
						if ($scope.password == null) {
							var n = noty({
								text : 'Error: New Password must be entered',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;
						}
						if ($scope.confirmPassword == null) {
							var n = noty({
								text : 'Error: Confirm Password must be entered',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;
						}
						if ($scope.password != $scope.confirmPassword) {
							var n = noty({
								text : 'Error: Passwords donot match',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;
						}
						
						
						var data =

						{
							'fname' : $scope.fname,
							'lname' : $scope.lname,
							'email' : $scope.email,
							'username' : $scope.username,
							'address' : $scope.address,
							'password' : $scope.password,
							'city' : $scope.city,
							'state' : $scope.state,
							'country' : $scope.country,
							'postalCode' : $scope.postalCode,
							'phoneNumber' : $scope.phoneNumber,
							'accountNumber' : $scope.accountNumber + ' - ' + $scope.ifsccode 
							

						};

						$http
								.post(
										serverURL
												+ boomerangServer + "/api/server/userService/registerUser",
										data,
										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'User created successfully',
												layout : 'topRight',
												type : 'success'
											});
										
											//window.location = redirectToOrgDashURL+$scope.orgId;
											window.location = redirectToReviewrDashURL+sessionStorage.getItem('reviewerId');
										})
								.error(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'Error: '
														+ headers('BoomerangError'),
												layout : 'topRight',
												type : 'error'
											});

										});
						
						
					}
					
				}]);


app
.controller(
		'OrgAdminCreateCtrl',
		[
				'$scope',
				'$http',
				'$routeParams',
				function($scope, $http, $routeParams) {

					console.log('In OrgAdminCreateCtrl');
					
					$scope.orgId = $routeParams.orgId;
				    $scope.orgName =$routeParams.orgName;
					$scope.CreateOrgAdmin = function() {

						console.log('In CreateOrgAdmin');
						if ($scope.email == null){
							var n = noty({
								text : 'Error: Email must be entered',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;
						}
						if ($scope.password == null){
							var n = noty({
								text : 'Error: Password must be entered',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;
						}
								if ($scope.confirmPassword == null) {

							var n = noty({
								text : 'Error: Confirm Password must be entered',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;

						}
						if ($scope.password != $scope.confirmPassword) {
							var n = noty({
								text : 'Error: Passwords do not match',
								layout : 'topRight',
								type : 'error'
							});
							
							return;
						}
						
						
						$http
						.get(
								serverURL + boomerangServer+"/api/server/organization/getOrgById/"+ $routeParams.orgId,
								{
									headers : {
										'Content-Type' : 'application/json; charset=UTF-8'
									}
								})
						.success(
								function(data, status, headers,
										config) {
									
									//$scope.currentOrg=data;
									
									//$routeParams.orgName=$scope.currentOrg.orgName;
									
									$scope.currentOrg=data;
									
									var currentOrgName=$scope.currentOrg.orgName;
									console.log($scope.currentOrg.orgName);
									$scope.createOrgAdmin(currentOrgName);
									//$scope.currentEvents = data;

								})
						.error(
								function(data, status, headers,
										config) {
									if(status == 401){
										checkStatus();
									}
									else{

									var n = noty({
										text : 'Error: '
												+ headers('BoomerangError'),
										layout : 'topRight',
										type : 'error'
									});
									}

								});

						$scope.createOrgAdmin=function(currentOrgName){
						var data =

						{
							'fname' : $scope.fname,
							'lname' : $scope.lname,
							'email' : $scope.email,
							'username' : $scope.email,
							'title' : $scope.title,
							'password' : $scope.password,
							'orgId' : $scope.orgId,
							

						};

						$http
								.post(
										serverURL
												+ boomerangServer+"/api/server/adminmanagerservice/addAdmin",
										data,
										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'Administrator created successfully',
												layout : 'topRight',
												type : 'success'
											});
										
											//window.location = redirectToOrgDashURL+$scope.orgId;
											window.location = redirectToAdminDetailURL+"username/"+$scope.email+"/orgName/"+currentOrgName;
										})
								.error(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'Error: '
														+ headers('BoomerangError'),
												layout : 'topRight',
												type : 'error'
											});

										});

					}}

				}

		]);


app
.controller(
		'AdminloginCtrl',
		[
				'$scope',
				'$http',

				function($scope, $http) {

					console.log('In AdminloginCtrl');
					$scope.Authentication = function() {
						
						if ($scope.username == null) {
							var n = noty({
								text : 'Error: You must enter Username',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;
						}
						if ($scope.password == null) {
							var n = noty({
								text : 'Error: You must enter Password',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;
						}
						
						

						var data =

						{
							
							'username' : $scope.username,
							'password' : $scope.password,
							

						};
						console.log(data);
						
						

						$http
								.post(
										serverURL
												+ boomerangServer+"/api/server/reviewerManagerService/login",
										data,
										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {

											
											
											var userName=$scope.username;
											
										
											$scope.getOrgIdUsinguserName(userName);
											
											
											
											
										})
								.error(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'Please Check Your Login Credentials !!'
														,
												layout : 'topRight',
												killer : 'true',
												type : 'error'
											});

										});

			
					
					
					}
					
					
					
					
					$scope.getOrgIdUsinguserName = function(userName){
						
						$http
						.get(
								serverURL + boomerangServer+"/api/server/reviewerManagerService/getRLReviewer/userName/"+ userName,
								{
									headers : {
										'Content-Type' : 'application/json; charset=UTF-8'
									}
								})
						.success(
								function(data, status, headers,
										config) {
									
									var n = noty({
										text : ' Login successfully',
										layout : 'topRight',
										type : 'success'
									});
									$scope.Reviewerdetails=data;
									console.log(data);
									var reviewerId=$scope.Reviewerdetails.reviewerId;
									window.location = redirectToReviewrDashURL + reviewerId;
									
									//$scope.currentEvents = data;

								})
						.error(
								function(data, status, headers,
										config) {

									var n = noty({
										text : 'Please Check Your Login Credentials !!',
										layout : 'topRight',
										type : 'error'
									});

								});

						
					}
					

				}

		]);



      app
		.controller(
				'OrgDashboardCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http,$routeParams) {

							console.log('Inside OrgDashboardCtrl');
							
							//currentEvents
							//UpcomingEvents
							//PreviousEvents
							//OrgAdvertisers
							
							sessionStorage.reviewerId= $routeParams.reviewerId;
							console.log(sessionStorage.reviewerId);
							
							$http
							.get(
									serverURL + boomerangServer+"/api/server/reviewerManagerService/getReports",
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {
										
										console.log(data);
										
										$scope.reports=data;
										
										
									})
							.error(
									function(data, status, headers,
											config) {
										if(status == 401){
											checkStatus();
										}
										else{
											
										
										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});
										}

									});
							
							$http
							.get(
									serverURL + boomerangServer+"/api/server/reviewerManagerService/getHistoricalReports",
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {
										
										console.log(data);
										
										$scope.historicalReports=data;
										
										
									})
							.error(
									function(data, status, headers,
											config) {
										if(status == 401){
											checkStatus();
										}
										else{
											
										
										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});
										}

									});							
							
							
							$http
							.get(
									serverURL + boomerangServer+"/api/server/UserManagerService/getAllUsers",
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {
										
										console.log(data);
										
										$scope.users=data;
										
										
									})
							.error(
									function(data, status, headers,
											config) {
										if(status == 401){
											checkStatus();
										}
										else{
											
										
										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});
										}

									});
							
							
							$scope.UpdateReport = function(reportId,status,notes) {
								
								
								
								var data =

								{
									'reportId' : reportId,
									'notes' : notes,
									'status' : status,
									

								};

								$http
										.post(
												serverURL
														+ boomerangServer + "/api/server/reviewerManagerService/updateReport",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Violation Report approved',
														layout : 'topRight',
														type : 'success'
													});
												
													//window.location = redirectToOrgDashURL+$scope.orgId;
													window.location.reload();
												})
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														type : 'error'
													});

												});
								
								
							}							
							
							
							
							
						} ]);







      
      app
		.controller(
				'OrgDetailCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {

							var getOrgSvc = "";
							if($routeParams.orgId == null){
								getOrgSvc = boomerangServer+"/api/server/organization/getOrg/"
									+ $routeParams.orgName + "?withLogo=true";	
							}else{
								getOrgSvc = boomerangServer+"/api/server/organization/getOrgById/"
									+ $routeParams.orgId;
							}
							
							$http
									.get(
											serverURL
													+ getOrgSvc,
											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												$scope.org = data;

											})
									.error(
											function(data, status, headers,
													config) {
												if(status == 401){
													checkStatus();
												}
												else{

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													type : 'error'
												});
												}

											});


							var listAdminSvc = "";
							
							if($routeParams.orgId == null){
								listAdminSvc = boomerangServer+"/api/server/adminmanagerservice/listAdminsForOrgName/orgName/"
									+ $routeParams.orgName;
							}else{
								listAdminSvc =boomerangServer+"/api/server/adminmanagerservice/listAdminsForOrgId/orgId/"
								+ $routeParams.orgId;
							}
							
							$http
									.get(
											serverURL
													+ listAdminSvc,
											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												$scope.adminlist = data;

											})
									.error(
											function(data, status, headers,
													config) {

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													type : 'error'
												});

											});
							
							
							
							
							
							
							
							
							
							
							
							

							$scope.deleteadmin = function(username) {
								console.log(username);
								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/adminmanagerservice/deleteAdmin/username/"
														+ username,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													
													
													var n = noty({
														text : 'Admin  deleted successfully',
														layout : 'topRight',
														type : 'success'
													});
													//window.location = redirectToOrgDashURL+$scope.adminlist.orgId;
													
												window.location.reload();

												})

										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														type : 'error'
													});

												});

							}
							
							
							
							
							
							$scope.updateOrganization = function() {
								console.log('Inside updateOrganization function');
								
								$scope.$apply(function() {
									document.alert(document
											.getElementById("logoPath").value);
									$scope.logoPath = document
											.getElementById("logoPath").value;
								});
								
								
								var logoPath = sessionStorage
								.getItem("logoPath");
								
								var data =

								{

										'orgName' : $scope.org.orgName,
										'orgId' : $scope.org.orgId,
										'addressLine1' : $scope.org.addressLine1,
										'url' : $scope.org.url,
										'displayName' : $scope.org.displayName,
										'dateCreated' : $scope.org.dateCreated,
										'dateModified' : $scope.org.dateModified,
										'status' : $scope.org.status,
										'attributes' : $scope.attributes,
										'addressLine1' : $scope.org.addressLine1,
										'addressLine2' : $scope.org.addressLine2,
										'city' : $scope.org.city,
										'state' : $scope.org.state,
										'postalCode' : $scope.org.postalCode,
										'country' : $scope.org.country,
										'revenuePercentage' : $scope.org.revenuePercentage,
										'additionalNotes' : $scope.org.additionalNotes,
										'logoPath' : logoPath

								};

								console.log(data);

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/organization/updateOrg",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Organization Updated  successfully',
														layout : 'topRight',
														type : 'success'
													});
													
													//sessionStorage.clear(logoPath);
													window.location = redirectToOrgDetailURL+$scope.org.orgName;
															window.location.reload();

												})

										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}
						} ]);
      
      
      
      
      
      
      
      app
		.controller(
				'AdminPwdCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {
							$scope.username = $routeParams.username;

							console.log('Inside AdminPwdCtrl');
							
							$http
							.get(
									serverURL
											+ boomerangServer+"/api/server/organization/getOrg/"
											+ $routeParams.orgName+"?withLogo=true",
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {

										$scope.currentOrg = data;
										console.log(data);
										// $scope.currentEvents =
										// data;

									})
							.error(
									function(data, status, headers,
											config) {
										if(status == 401){
											checkStatus();
										}
										else{
										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});
										}

									});
							
						

							$scope.adminpassword = function() {
								
								
								if ($scope.oldpassword == null) {
									var n = noty({
										text : 'Error: Old Password needs to be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								if ($scope.password == null) {
									var n = noty({
										text : 'Error: New Password must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.confirmpwd == null) {
									var n = noty({
										text : 'Error: Confirm Password must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.password != $scope.confirmpwd) {
									var n = noty({
										text : 'Error: Passwords donot match',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								$http
								.get(
										serverURL
												+ boomerangServer+"/api/server/adminmanagerservice/getAdmin/userName/"
												+ $routeParams.username,
												

										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {

											$scope.admin=data;
											if($scope.oldpassword==$scope.admin.password){
												$scope.resetPassword();
												}
												else{
													var n = noty({
														text : 'Sorry ! Please check your old Password',
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});
													
																									
											}
										}

								)
								.error(
										function(data, status, headers,
												config) {
											if(status == 401){
												checkStatus();
											}
											else{

											var n = noty({
												text : 'Error: '
														+ headers('BoomerangError'),
												layout : 'topRight',
												killer : 'true',
												type : 'error'
											});
											}

										});
							}
							$scope.resetPassword = function(){

								

								$http
										.get(
												serverURL
														+ boomerangServer+"/api/server/adminmanagerservice/resetPassword/username/"
														+ $routeParams.username
														+ "/orgName/"
														+ $routeParams.orgName
														+ "/newPassword/"
														+ $scope.password,

												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Password changed successfully',
														layout : 'topRight',
														type : 'success'
													});
													window.history.back();
												}

										)
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}

						} ]);


      
      
      app
		.controller(
				'AdminlogoutCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {
							$scope.username = $routeParams.username;

							console.log('Inside AdminlogoutCtrl');

															

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/session/logout",
												

												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {
													window.location = redirectToReviewerLoginURL;

													var n = noty({
														text : 'Reviewer Logged out Successfully',
														layout : 'topRight',
														type : 'success'
													});
													
												}

										)
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							

						} ]);
      
      
      
      
      
      
     /* //////resetloginpassword controller//////
      
      
      app.controller('ResetLoginPasswordDemoCtrl', function($scope, $modal, $log) {

    		

    		$scope.open = function(size) {
    			

    			var modalInstance = $modal.open({
    				templateUrl : 'angularjs.html',
    				controller : 'ResetLoginPasswordModalInstanceCtrl',
    				size : size,
    				
    			});

    			modalInstance.result.then(function(eventName) {

    			}, function() {
    				$log.info('Modal dismissed at: ' + new Date());
    			});
    		};
    	});

    	app.controller('ResetLoginPasswordModalInstanceCtrl', function($scope, $modalInstance) {
    		$scope.endEventByObject = function() {
    			window.alert();

    			$modalInstance.close();
    		};

    		$scope.cancel = function() {
    			$modalInstance.dismiss('cancel');
    		};
    	});*/