





/*app
.controller(
		'OrgAdminCreateCtrl',
		[
				'$scope',
				'$http',
				'$routeParams',
				function($scope, $http, $routeParams) {

					console.log('In OrgAdminCreateCtrl');
					
					$scope.orgId = $routeParams.orgId;
				    $scope.orgName =$routeParams.orgName;
					$scope.CreateOrgAdmin = function() {

						console.log('In CreateOrgAdmin');
						if ($scope.email == null){
							var n = noty({
								text : 'Error: Email must be entered',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;
						}
						if ($scope.password == null){
							var n = noty({
								text : 'Error: Password must be entered',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;
						}
								if ($scope.confirmPassword == null) {

							var n = noty({
								text : 'Error: Confirm Password must be entered',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;

						}
						if ($scope.password != $scope.confirmPassword) {
							var n = noty({
								text : 'Error: Passwords do not match',
								layout : 'topRight',
								type : 'error'
							});
							
							return;
						}
						
						
						$http
						.get(
								serverURL + boomerangServer+"/api/server/organization/getOrgById/"+ $routeParams.orgId,
								{
									headers : {
										'Content-Type' : 'application/json; charset=UTF-8'
									}
								})
						.success(
								function(data, status, headers,
										config) {
									
									//$scope.currentOrg=data;
									
									//$routeParams.orgName=$scope.currentOrg.orgName;
									
									$scope.currentOrg=data;
									
									var currentOrgName=$scope.currentOrg.orgName;
									console.log($scope.currentOrg.orgName);
									$scope.createOrgAdmin(currentOrgName);
									//$scope.currentEvents = data;

								})
						.error(
								function(data, status, headers,
										config) {
									if(status == 401){
										checkStatus();
									}
									else{

									var n = noty({
										text : 'Error: '
												+ headers('BoomerangError'),
										layout : 'topRight',
										type : 'error'
									});
									}

								});

						$scope.createOrgAdmin=function(currentOrgName){
						var data =

						{
							'fname' : $scope.fname,
							'lname' : $scope.lname,
							'email' : $scope.email,
							'username' : $scope.email,
							'title' : $scope.title,
							'password' : $scope.password,
							'orgId' : $scope.orgId,
							

						};

						$http
								.post(
										serverURL
												+ boomerangServer+"/api/server/adminmanagerservice/addAdmin",
										data,
										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'Administrator created successfully',
												layout : 'topRight',
												type : 'success'
											});
										
											//window.location = redirectToOrgDashURL+$scope.orgId;
											window.location = redirectToAdminDetailURL+"username/"+$scope.email+"/orgName/"+currentOrgName;
										})
								.error(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'Error: '
														+ headers('BoomerangError'),
												layout : 'topRight',
												type : 'error'
											});

										});

					}}

				}

		]);*/


app
.controller(
		'AdminloginCtrl',
		[
				'$scope',
				'$http',

				function($scope, $http) {

					console.log('In AdminloginCtrl');
					$scope.Authentication = function() {
						
						if ($scope.username == null) {
							var n = noty({
								text : 'Error: You must enter Username',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;
						}
						if ($scope.password == null) {
							var n = noty({
								text : 'Error: You must enter Password',
								layout : 'topRight',
								killer : 'true',
								type : 'error'
							});
							return;
						}
						
						
						var data =

						{
							
							'username' : $scope.username,
							'password' : $scope.password,
							

						};
						console.log(data);
						
						

						$http
								.post(
										serverURL
												+ boomerangServer+"/api/server/UserManagerService/login",
										data,
										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {

											
											
											var userName=$scope.username;
											console.log(userName);
										
											$scope.getOrgIdUsinguserName(userName);
											
											
											
											
										})
								.error(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'Please Check Your Login Credentials !!'
														,
												layout : 'topRight',
												killer : 'true',
												type : 'error'
											});

										});

			
					
					
					}
					
					
					
					
					$scope.getOrgIdUsinguserName = function(userName){
						
						$http
						.get(
								serverURL + boomerangServer+"/api/server/UserManagerService/getRLUser/userName/"+ userName,
								{
									headers : {
										'Content-Type' : 'application/json; charset=UTF-8'
									}
								})
						.success(
								function(data, status, headers,
										config) {
									
									var n = noty({
										text : ' Login successfully',
										layout : 'topRight',
										type : 'success'
									});
									console.log(data);
									$scope.Userdetails=data;
									var userId=$scope.Userdetails.userId;
									window.location = redirectToUserDashURL + userId;
									
									//$scope.currentEvents = data;

								})
						.error(
								function(data, status, headers,
										config) {

									var n = noty({
										text : 'Please Check Your Login Credentials !!',
										layout : 'topRight',
										type : 'error'
									});

								});

						
					}
					

				}

		]);



      app
		.controller(
				'OrgDashboardCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http,$routeParams) {

							console.log('Inside OrgDashboardCtrl');
							
							$scope.SubmitReport = function(){
								
								
								
								
								console.log($scope)
							    var data =

								{
									"startmark" : document.getElementById('startMark').value,
									"vehicleNumber" : $scope.vehicleNumber,
									"location" : $scope.location,
									"camId" : $scope.camId,
									"violationType" : $scope.violationType

								};

								$http
										.post(
												serverURL
														+ boomerangServer + "/api/server/userService/reportViolation",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Reported submitted successfully',
														layout : 'topRight',
														type : 'success'
													});
												
													//window.location = redirectToOrgDashURL+$scope.orgId;
													window.location.reload();
												})
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														type : 'error'
													});

												});
								
								
							}
							
							
							$scope.fetchVideo = function(){
								
								var dateStr = ''+new Date($scope.startDate).getDate()+'_'+new Date($scope.startDate).getMonth()+'_'+new Date($scope.startDate).getFullYear()+'_'+new Date($scope.startDate).getHours();
								
								$http
								.get(
										serverURL + magicSpyCamServer+"/api/server/UserManagerService/fetchVideoURL?day="+ dateStr,
										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {
											
											/*var n = noty({
												text : ' ',
												layout : 'topRight',
												type : 'success'
											});*/
											console.log(data);
											$scope.myVideo=data;
											
											document.getElementById("myVideo").src = ''+data;
											document.getElementById("myVideo").load();
											document.getElementById("myVideo").play();

										})
								.error(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'Unable to fetch video !!',
												layout : 'topRight',
												type : 'error'
											});

										});	
								
							} 
							
							//currentEvents
							//UpcomingEvents
							//PreviousEvents
							//OrgAdvertisers
							
							sessionStorage.userId= $routeParams.userId;
							console.log(sessionStorage.userId);
							
							$http
							.get(
									serverURL + boomerangServer+"/api/server/UserManagerService/getRLUserById/userId/"+ $routeParams.userId,
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {
										
										/*var n = noty({
											text : ' ',
											layout : 'topRight',
											type : 'success'
										});*/
										console.log(data);
										$scope.userdetails=data;
										
									})
							.error(
									function(data, status, headers,
											config) {

										var n = noty({
											text : 'Please Check Your Login Credentials !!',
											layout : 'topRight',
											type : 'error'
										});

									});
							
							
							$http
							.get(
									serverURL + boomerangServer+"/api/server/reviewerManagerService/getReportsForUser/"+ $routeParams.userId,
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {
										
										/*var n = noty({
											text : ' ',
											layout : 'topRight',
											type : 'success'
										});*/
										console.log(data);
										$scope.userReports=data;
										
										//$scope.currentEvents = data;

									})
							.error(
									function(data, status, headers,
											config) {

										var n = noty({
											text : 'Error while fetching user reports !!',
											layout : 'topRight',
											type : 'error'
										});

									});
							
							$scope.ShowEditDiv = false;
							 $scope.EditProfile = function () {
					                //If DIV is visible it will be hidden and vice versa.
					                $scope.ShowEditDiv = $scope.ShowEditDiv ? false : true;
					            }
							 
							 
							 $scope.ShowResetPassword = false;
							 $scope.EditPassword = function () {
					                //If DIV is visible it will be hidden and vice versa.
					                $scope.ShowResetPassword = $scope.ShowResetPassword ? false : true;
					            }
							
							
							$scope.updateUser=function(){
								
								console.log("Inside updateUser");
								
								var data =

								{
									'userId' : $routeParams.userId,
									'username' : $scope.userdetails.username,
									'email' : $scope.userdetails.email,
									'fname' : $scope.userdetails.fname,
									'lname' : $scope.userdetails.lname,
									'phoneNumber':$scope.userdetails.phoneNumber,
									'address' :$scope.userdetails.address,
									'city' :$scope.userdetails.city,
									'state':$scope.userdetails.state,
									'postalCode':$scope.userdetails.postalCode,
									'dob':$scope.userdetails.dob,
									'accountNumber':$scope.userdetails.accountNumber

								};
								console.log(data);
								
								$http
								.post(
										serverURL
												+ boomerangServer+"/api/server/UserManagerService/updateAdmin",
										data,
										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {

											 window.location.reload();
											
											
										})
								.error(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'Unable to Update User Details !!'
														,
												layout : 'topRight',
												killer : 'true',
												type : 'error'
											});

										});
								
								
								
								
								
							}
							
							
$scope.adminpassword = function() {
								
								
								if ($scope.oldpassword == null) {
									var n = noty({
										text : 'Error: Old Password needs to be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								if ($scope.password == null) {
									var n = noty({
										text : 'Error: New Password must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.confirmpwd == null) {
									var n = noty({
										text : 'Error: Confirm Password must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.password != $scope.confirmpwd) {
									var n = noty({
										text : 'Error: Passwords donot match',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								$http
								.get(
										serverURL
												+ boomerangServer+"/api/server/UserManagerService/getRLUser/userName/"
												+ $scope.userdetails.username,
												

										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {

											$scope.user=data;
											if($scope.oldpassword==$scope.user.password){
												$scope.resetPassword();
												}
												else{
													var n = noty({
														text : 'Sorry ! Please check your old Password',
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});
													
																									
											}
										}

								)
								.error(
										function(data, status, headers,
												config) {
											if(status == 401){
												checkStatus();
											}
											else{

											var n = noty({
												text : 'Error: '
														+ headers('BoomerangError'),
												layout : 'topRight',
												killer : 'true',
												type : 'error'
											});
											}

										});
							}
							$scope.resetPassword = function(){

								

								$http
										.get(
												serverURL
														+ boomerangServer+"/api/server/UserManagerService/resetPassword/username/"
														+ $scope.userdetails.username
														+ "/userId/"
														+ $routeParams.userId
														+ "/newPassword/"
														+ $scope.password,

												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Password changed successfully',
														layout : 'topRight',
														type : 'success'
													});
													 window.location.reload();
												}

										)
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}
		
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
							
															
							
						} ]);







      
    /* app
		.controller(
				'OrgDetailCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {

							var getOrgSvc = "";
							if($routeParams.orgId == null){
								getOrgSvc = boomerangServer+"/api/server/organization/getOrg/"
									+ $routeParams.orgName + "?withLogo=true";	
							}else{
								getOrgSvc = boomerangServer+"/api/server/organization/getOrgById/"
									+ $routeParams.orgId;
							}
							
							$http
									.get(
											serverURL
													+ getOrgSvc,
											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												$scope.org = data;

											})
									.error(
											function(data, status, headers,
													config) {
												if(status == 401){
													checkStatus();
												}
												else{

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													type : 'error'
												});
												}

											});


							var listAdminSvc = "";
							
							if($routeParams.orgId == null){
								listAdminSvc = boomerangServer+"/api/server/adminmanagerservice/listAdminsForOrgName/orgName/"
									+ $routeParams.orgName;
							}else{
								listAdminSvc =boomerangServer+"/api/server/adminmanagerservice/listAdminsForOrgId/orgId/"
								+ $routeParams.orgId;
							}
							
							$http
									.get(
											serverURL
													+ listAdminSvc,
											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												$scope.adminlist = data;

											})
									.error(
											function(data, status, headers,
													config) {

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													type : 'error'
												});

											});
							
							
							
							
							
							
							
							
							
							
							
							

							$scope.deleteadmin = function(username) {
								console.log(username);
								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/adminmanagerservice/deleteAdmin/username/"
														+ username,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													
													
													var n = noty({
														text : 'Admin  deleted successfully',
														layout : 'topRight',
														type : 'success'
													});
													//window.location = redirectToOrgDashURL+$scope.adminlist.orgId;
													
												window.location.reload();

												})

										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														type : 'error'
													});

												});

							}
							
							
							
							
							
							$scope.updateOrganization = function() {
								console.log('Inside updateOrganization function');
								
								$scope.$apply(function() {
									document.alert(document
											.getElementById("logoPath").value);
									$scope.logoPath = document
											.getElementById("logoPath").value;
								});
								
								
								var logoPath = sessionStorage
								.getItem("logoPath");
								
								var data =

								{

										'orgName' : $scope.org.orgName,
										'orgId' : $scope.org.orgId,
										'addressLine1' : $scope.org.addressLine1,
										'url' : $scope.org.url,
										'displayName' : $scope.org.displayName,
										'dateCreated' : $scope.org.dateCreated,
										'dateModified' : $scope.org.dateModified,
										'status' : $scope.org.status,
										'attributes' : $scope.attributes,
										'addressLine1' : $scope.org.addressLine1,
										'addressLine2' : $scope.org.addressLine2,
										'city' : $scope.org.city,
										'state' : $scope.org.state,
										'postalCode' : $scope.org.postalCode,
										'country' : $scope.org.country,
										'revenuePercentage' : $scope.org.revenuePercentage,
										'additionalNotes' : $scope.org.additionalNotes,
										'logoPath' : logoPath

								};

								console.log(data);

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/organization/updateOrg",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Organization Updated  successfully',
														layout : 'topRight',
														type : 'success'
													});
													
													//sessionStorage.clear(logoPath);
													window.location = redirectToOrgDetailURL+$scope.org.orgName;
															window.location.reload();

												})

										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}
						} ]);*/
      
      
      
      
      
      
      
     /* app
		.controller(
				'AdminPwdCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {
							$scope.username = $routeParams.username;

							console.log('Inside AdminPwdCtrl');
							
							$http
							.get(
									serverURL
											+ boomerangServer+"/api/server/organization/getOrg/"
											+ $routeParams.orgName+"?withLogo=true",
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {

										$scope.currentOrg = data;
										console.log(data);
										// $scope.currentEvents =
										// data;

									})
							.error(
									function(data, status, headers,
											config) {
										if(status == 401){
											checkStatus();
										}
										else{
										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});
										}

									});
							
						

							$scope.adminpassword = function() {
								
								
								if ($scope.oldpassword == null) {
									var n = noty({
										text : 'Error: Old Password needs to be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								if ($scope.password == null) {
									var n = noty({
										text : 'Error: New Password must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.confirmpwd == null) {
									var n = noty({
										text : 'Error: Confirm Password must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.password != $scope.confirmpwd) {
									var n = noty({
										text : 'Error: Passwords donot match',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								$http
								.get(
										serverURL
												+ boomerangServer+"/api/server/adminmanagerservice/getAdmin/userName/"
												+ $routeParams.username,
												

										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {

											$scope.admin=data;
											if($scope.oldpassword==$scope.admin.password){
												$scope.resetPassword();
												}
												else{
													var n = noty({
														text : 'Sorry ! Please check your old Password',
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});
													
																									
											}
										}

								)
								.error(
										function(data, status, headers,
												config) {
											if(status == 401){
												checkStatus();
											}
											else{

											var n = noty({
												text : 'Error: '
														+ headers('BoomerangError'),
												layout : 'topRight',
												killer : 'true',
												type : 'error'
											});
											}

										});
							}
							$scope.resetPassword = function(){

								

								$http
										.get(
												serverURL
														+ boomerangServer+"/api/server/adminmanagerservice/resetPassword/username/"
														+ $routeParams.username
														+ "/orgName/"
														+ $routeParams.orgName
														+ "/newPassword/"
														+ $scope.password,

												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Password changed successfully',
														layout : 'topRight',
														type : 'success'
													});
													window.history.back();
												}

										)
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}

						} ]);
*/

      
      
      app
		.controller(
				'AdminlogoutCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {
							$scope.username = $routeParams.username;

							console.log('Inside AdminlogoutCtrl');

															

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/session/logout",
												

												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {
													window.location = redirectToUserLoginURL;

													var n = noty({
														text : 'User Logged out Successfully',
														layout : 'topRight',
														type : 'success'
													});
													
												}

										)
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							

						} ]);
      
      
      
      
      
      
    