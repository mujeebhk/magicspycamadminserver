

var app = angular.module('bmgApp', [ 'ngRoute', 'ui.bootstrap' ]);

app
		.config(function($routeProvider, $locationProvider) {

			$routeProvider
			/*.when('/sudash', {
				templateUrl : 'partials/SuperAdminDashboard.html',
				controller : 'SuDashboardListCtrl'
			})*/
			/*.when('/orgdash/orgName/:orgName', {
				templateUrl : 'partials/OrganizationDashboard.html',
				controller : 'OrgDashboardCtrl'
			})*/
			.when('/userdash/userId/:userId', {
				templateUrl : 'partials/UserDashboard.html',
				controller : 'OrgDashboardCtrl'
			})
			.when('/new', {
				templateUrl : 'partials/OrganizationCreate.html',
				controller : 'OrganizationNewCtrl'
			})
			.when('/orgs/:orgName', {
				templateUrl : 'partials/OrganizationDetail.html',
				controller : 'OrgDetailCtrl',
			})
			.when('/updateorgByName/:orgName', {
				templateUrl : 'partials/OrganizationUpdate.html',
				controller : 'OrgDetailCtrl',
			})
			.when('/superadmins/:username', {
				templateUrl : 'partials/SuperAdminDetails.html',
				controller : 'SuperAdminEditCtrl',
			})
			.when('/Admin', {
				templateUrl : 'partials/org-admin.html',
				controller : 'OrganizationAdminCtrl'
			/*
			 * }).when('/Pwd/:username', { templateUrl :
			 * 'partials/SuperAdminResetPassword.html', controller :
			 * 'OrganizationPwdCtrl'
			 */
			})
			.when(
					'/adminPasswordReset/username/:username/orgName/:orgName',
					{
						templateUrl : 'partials/OrganizationAdminResetPassword.html',
						controller : 'AdminPwdCtrl'
					})
			.when('/superAdminPasswordReset/username/:username', {
				templateUrl : 'partials/SuperAdminResetPassword.html',
				controller : 'SuperAdminResetPwdCtrl'
			})
			/*.when('/orgdash/:orgId/orgName/:orgName', {
				templateUrl : 'partials/OrganizationDashboard.html',
				controller : 'OrgDashboardCtrl'
			})*/
			.when('/adsuuser', {
				templateUrl : 'partials/SuperAdminCreate.html',
				controller : 'adsuperadminDashboardCtrl'
			})
			.when('/CurrentPollCreate/orgName/:orgName/orgId/:orgId/eventId/:eventId', {
				templateUrl : 'partials/PollCreate.html',
				controller : 'pollDashboardCtrl'
			})
			.when('/UpcomingPollCreate/orgName/:orgName/orgId/:orgId/eventId/:eventId', {
				templateUrl : 'partials/UpcomingPollCreate.html',
				controller : 'pollDashboardCtrl'
			})
			.when('/updateadvertiserImglabelNotes/orgName/:orgName/AvertiserId/:advertiserId/transactionId/:transactionId', {
				templateUrl : 'partials/AdvertisementUpdateViaCreatePoll.html',
				controller : 'adLabelNotesUpdateCtrl'
			})
			.when('/newevent/:orgId', {
				templateUrl : 'partials/EventCreate.html',
				controller : 'CreateeventDashboardCtrl'
			})
			.when('/newadvt/:orgId', {
				templateUrl : 'partials/SponsorCreate.html',
				controller : 'createSponsorDashboardCtrl'
			})
			.when('/CurrentEventDetail/eventId/:eventId/orgName/:orgName',
					{
						templateUrl : 'partials/EventDetails.html',
						controller : 'CurrentEventDetailsCtrl'
					})
					.when('/UpcomingEventDetail/eventId/:eventId/orgName/:orgName',
					{
						templateUrl : 'partials/UpcomingEventDetails.html',
						controller : 'CurrentEventDetailsCtrl'
					})
			.when(
					'/PreviousEventDetail/eventId/:eventId/orgName/:orgName',
					{
						templateUrl : 'partials/PreviousEventDetails.html',
						controller : 'PreviousEventDetailsCtrl'

					})
			.when(
					'/UpdateUpcomingEvent/eventId/:eventId/orgName/:orgName',
					{
						templateUrl : 'partials/EventUpdate.html',
						controller : 'CurrentEventDetailsCtrl'
					
					})
					
			.when('/pollupdate/:pollId/orgId/:orgId', {
				templateUrl : 'partials/PollUpdate.html',
				controller : 'pollDetailsCtrl'

			

			})
			.when('/polldetails/:pollId/orgId/:orgId', {
				templateUrl : 'partials/PollDetails.html',
				controller : 'pollDetailsCtrl'
			})
			.when('/UpcomingPolldetails/:pollId/orgId/:orgId', {
				templateUrl : 'partials/UpcomingPollDetails.html',
				controller : 'pollDetailsCtrl'
			})
			
			
			.when('/admindetail/username/:username/orgName/:orgName', {
				templateUrl : 'partials/AdminDetails.html',
				controller : 'AdminDetailsCtrl'
			})
			.when(
					'/updateOrgAdminById/username/:username/orgName/:orgName',
					{
						templateUrl : 'partials/AdminUpdate.html',
						controller : 'AdminDetailsCtrl'

					})
			.when('/advertiserDetails/advertiserId/:advertiserId/orgName/:orgName',
					{
						templateUrl : 'partials/sponsorDetails.html',
						controller : 'sponsorDetailCtrl'
					})
			.when(
					'/getSponsorsForUpdateSponsors/advertiserId/:advertiserId/orgName/:orgName',
					{
						templateUrl : 'partials/SponsorUpdate.html',
						controller : 'SponsorEditCtrl'
					
					}).when('/SuperAdminLogin', {
				templateUrl : 'partials/SuperAdminLogin.html',
				controller : 'SuperadminloginCtrl'
			}).when('/adorgadmin/:orgId', {
				templateUrl : 'partials/OrgAdminCreate.html',
				controller : 'OrgAdminCreateCtrl'
			}).when('/AdminLogin', {
				templateUrl : 'partials/AdminLogin.html',
				controller : 'AdminloginCtrl'
			}).when('/getSuperAdminForUpdate/username/:username', {
				templateUrl : 'partials/SuperAdminUpdate.html',
				controller : 'SuperAdminEditCtrl'
			}).when('/SellAd/orgName/:orgName/orgId/:orgId/advId/:advId', {
				templateUrl : 'partials/SellAd.html',
				controller : 'SellAdInitiateCtrl'
			}).when('/Logout', {
				templateUrl : 'partials/AdminLogin.html',
				controller : 'AdminlogoutCtrl'
			
			}).when('/dashboard', {
				templateUrl : 'partials/OrganizationDashboard.html',
				controller : 'BreadCrumbCtrl'
			}).when('/payByCheck_success/txId/:txId/orgName/:orgName', {
				templateUrl : 'partials/payByCheck_success.html',
				controller : 'payByCheck_SuccessCtrl'
					
			}).when('/sponsorList', {
				
					
			}).otherwise({
				redirectTo : '/userdash/userId/:userId'
			});
});

/*app.controller('ModalDemoCtrl', function($scope, $modal, $log) {

	$scope.fileName = "";

	$scope.open = function(size) {

		var modalInstance = $modal.open({
			templateUrl : 'angularjs.html',
			controller : 'ModalInstanceCtrl',
			size : size,
			resolve : {
				fileName : function() {
					return $scope.fileName;
				}
			}
		});

		modalInstance.result.then(function(fileName) {

		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
});

app.controller('ModalInstanceCtrl', function($scope, $modalInstance, fileName) {

	$scope.fileName = fileName;

	$scope.ok = function() {
		$scope.logoPath = $scope.fileName;
		$modalInstance.close($scope.fileName);
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
});*/

app
		.controller(
				'sponsorDetailCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {

							console.log('Inside sponsorDetailCtrl');
							
							
							$http
							.get(
									serverURL
											+ boomerangServer+"/api/server/organization/getOrg/"
											+ $routeParams.orgName+"?withLogo=true",
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {

										$scope.currentOrg = data;
										console.log(data);
										// $scope.currentEvents =
										// data;

									})
							.error(
									function(data, status, headers,
											config) {
										if(status == 401){
											checkStatus();
										}
										else{

										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});
										}
									});

							
							
							var URL = serverURL
									+ boomerangServer+"/api/server/advertiserservice/AdvertiserUsingAdvertiserIdAndOrgName/advertiserId/"
									+ $routeParams.advertiserId + "/orgName/"
									+ $routeParams.orgName;

							

							$http
									.get(
											URL,

											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												$scope.adr = data;
												var advertiserId=$scope.adr.advertiserId;
												$scope.FetchAdvertisement(advertiserId);
												$scope.FetchTransactionDetails(advertiserId);
											})
									.error(
											function(data, status, headers,
													config) {

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													killer : 'true',
													type : 'error'
												});

											});

							
							
							
							$scope.FetchAdvertisement=function(advertiserId){
								$scope.advertiserId=advertiserId;
								console.log($scope.advertiserId);
							$http
							.get(
									serverURL
											+ boomerangServer+"/api/server/advertismentservice/getAdvertisment/advertiserId/"
											+ $scope.advertiserId,

									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {
										
										console.log(data);
										$scope.advertisement=data;
										
										
									})
							.error(
									function(data, status, headers,
											config) {

										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											killer : 'true',
											type : 'error'
										});

									}); 
							
							}
							
							$scope.FetchTransactionDetails=function(advertiserId){
								$scope.advertiserId=advertiserId;
								console.log($scope.advertiserId);
							
							$http
							.get(
									serverURL
											+ boomerangServer+"/api/server/adPurchaseHelper/listOfTransactionsByAdvertiser/advertiserId/"
											+ $scope.advertiserId,

									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {
										
										console.log(data);
										$scope.transactionDetails=data;
										
									})
							.error(
									function(data, status, headers,
											config) {

										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											killer : 'true',
											type : 'error'
										});

									}); 

							
							}
							
							
							
							
							
				} ]);
							
							
						


/*
 * app .controller( 'SponsorUpdateCtrl', [ '$scope', '$http', '$routeParams',
 * function($scope, $http, $routeParams) {
 * 
 * console.log('Inside SponsorUpdateCtrl');
 * 
 * $scope.UpdateSponsor = function() {
 * 
 * var data = {
 * 
 * 'typeOfBusiness' : $scope.typeOfBusiness, 'addressLine1' :
 * $scope.addressLine1, 'addressLine2' : $scope.addressLine2, 'city' :
 * $scope.city, 'state' : $scope.state, 'contactPhoneAreacode' :
 * $scope.contactPhoneAreacode, 'country' : $scope.country, 'contactEmail' :
 * $scope.contactEmail, 'contactPhoneNumber' : $scope.contactPhoneNumber,
 * 'contactFname' : $scope.contactFname, 'contactLname' : $scope.contactLname,
 * 'postalCode' : $scope.postalCode, 'additionalNotes' : $scope.additionalNotes,
 * 
 * 'businessName' : $scope.businessName,
 * 
 * 'orgId' : adr.orgId, };
 * 
 * console.log(data);
 * 
 * $http .post( serverURL +
 * "/boomerang/api/server/advertiserservice/updateAdvertiser", data, { headers : {
 * 'Content-Type' : 'application/json; charset=UTF-8' } }) .success(
 * function(data, status, headers, config) {
 * 
 * var n = noty({ text : 'Sponsor Updated successfully', layout : 'top', type :
 * 'success' }); }) .error( function(data, status, headers, config) {
 * 
 * var n = noty({ text : 'Error: ' + headers('BoomerangError'), layout : 'top',
 * killer:'true',type : 'error' });
 * 
 * }); } } ]);
 */

app
		.controller(
				'SponsorEditCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {

							console.log('Inside sponsorEditCtrl');
							
							
							$http
							.get(
									serverURL
											+ boomerangServer+"/api/server/organization/getOrg/"
											+ $routeParams.orgName+"?withLogo=true",
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {

										$scope.currentOrg = data;
										console.log(data);
										// $scope.currentEvents =
										// data;

									})
							.error(
									function(data, status, headers,
											config) {
										if(status == 401){
											checkStatus();
										}
										else{

										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});
										}
									});

							
							

							$http
									.get(
											serverURL
													+ boomerangServer+"/api/server/advertiserservice/AdvertiserUsingAdvertiserIdAndOrgName/advertiserId/"
													+ $routeParams.advertiserId
													+ "/orgName/"
													+ $routeParams.orgName,

											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												$scope.adr = data;
												var advertiserId=$scope.adr.advertiserId;
												$scope.FetchAdvertisement(advertiserId);

											})
									.error(
											function(data, status, headers,
													config) {

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													killer : 'true',
													type : 'error'
												});

											});
							
							
							$scope.FetchAdvertisement=function(advertiserId){
								$scope.advertiserId=advertiserId;
								//console.log($scope.advertiserId);
							$http
							.get(
									serverURL
											+ boomerangServer+"/api/server/advertismentservice/getAdvertisment/advertiserId/"
											+ $scope.advertiserId,

									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {
										$scope.advertisement=data;
										

									})
							.error(
									function(data, status, headers,
											config) {

										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											killer : 'true',
											type : 'error'
										});

									}); 
							}
							
							

							$scope.updateSponsor = function() {
								console.log('Inside updateSponsor function');
								
								/*$scope.$apply(function() {
									document.alert(document
											.getElementById("logoPath").value);
									$scope.logoPath = document
											.getElementById("logoPath").value;
								});*/
								
								
								if ($scope.adr.businessName.length == 0) {
									var n = noty({
										text : 'Error: Business Name must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if($scope.adr.contactFname.length==0) {
									var n = noty({
										text : 'Error: Contact Person First Name must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.adr.contactEmail.length == 0) {
									var n = noty({
										text : 'Error: Email must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								
								
								var logoPath = sessionStorage
								.getItem("fileData");
								console.log(logoPath);

								var data =

								{

									'advertiserId' : $scope.adr.advertiserId,
									'businessName' : $scope.adr.businessName,
									'contactFname' : $scope.adr.contactFname,
									'contactLname' : $scope.adr.contactLname,
									'contactEmail' : $scope.adr.contactEmail,
									'contactPhoneAreacode' : $scope.adr.contactPhoneAreacode,
									'contactPhoneNumber' : $scope.adr.contactPhoneNumber,
									'addressLine1' : $scope.adr.addressLine1,
									'addressLine2' : $scope.adr.addressLine2,
									'city' : $scope.adr.city,
									'state' : $scope.adr.state,
									'postalCode' : $scope.adr.postalCode,
									'country' : $scope.adr.country,
									'typeOfBusiness' : $scope.adr.typeOfBusiness,
									'additionalNotes' : $scope.adr.additionalNotes,
									'orgId' : $scope.adr.orgId,
									'logoPath' : sessionStorage.getItem("fileData"),
									'adRedirectUrl':$scope.advertisement.adRedirectUrl,
									'label':$scope.advertisement.label,
									'notes':$scope.advertisement.notes,
									'advertisementId':$scope.advertisement.advertisementId
									

								};

								console.log(data);

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/advertiserservice/updateAdvertiser",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Sponsor Updated  successfully',
														layout : 'topRight',
														type : 'success'
													});
													sessionStorage
													.removeItem("fileData");
													window.location = redirectToSponsorDetailsURL
															+ $scope.adr.advertiserId
															+ "/orgName/"
															+ $scope.adr.orgName;
													// window.location.reload();

												})

										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}

						} ]);




/*
 * app .controller( 'OrganizationAdminCtrl', [ '$scope', '$http',
 * function($scope, $http) {
 * 
 * $scope.CreateOrg = function() {
 * 
 * if ($scope.orgName == null){ var n = noty({ text : 'Error: Organization name
 * must be entered', layout : 'topRight', killer : 'true', type : 'error' });
 * return; } if ($scope.displayName == null){ var n = noty({ text : 'Error:
 * Organization Display name must be entered', layout : 'topRight', killer :
 * 'true', type : 'error' }); return; }
 * 
 * var data = { 'orgName' : $scope.orgName, 'displayName' : $scope.displayName,
 * 'addressLine1' : $scope.addressLine1, 'url' : $scope.url, 'displayName' :
 * $scope.displayName, 'dateCreated' : $scope.dateCreated, 'dateModified' :
 * $scope.dateModified, 'status' : $scope.status, 'attributes' :
 * $scope.attributes, 'addressLine1' : $scope.addressLine1, 'addressLine2' :
 * $scope.addressLine2, 'city' : $scope.city, 'state' : $scope.state,
 * 'postalCode' : $scope.postalCode, 'country' : $scope.country,
 * 'revenuePercentage' : $scope.revenuePercentage, 'additionalNotes' :
 * $scope.additionalNotes, 'logo' : $scope.logo };
 * 
 * $http .post( serverURL + "/boomerang/api/server/organization/createOrg",
 * data, { headers : { 'Content-Type' : 'application/json; charset=UTF-8' } })
 * .success( function(data, status, headers, config) {
 * 
 * var n = noty({ text : 'Organization created successfully', layout :
 * 'topRight', type : 'success' }); }) .error( function(data, status, headers,
 * config) {
 * 
 * var n = noty({ text : 'Error: ' + headers('BoomerangError'), layout :
 * 'topRight', killer:'true',type : 'error' });
 * 
 * }); } } ]);
 */

app
		.controller(
				'OrganizationNewCtrl',
				[
						'$scope',
						'$http',
						function($scope, $http) {

							$scope.$apply(function() {
								$scope.logoPath = document
										.getElementById("logoPath").value;
							});

							$scope.CreateOrg = function() {

								if ($scope.orgName == null) {
									var n = noty({
										text : 'Error: Organization name must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.displayName == null) {
									var n = noty({
										text : 'Error: Organization Display name must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ( $scope.revenuePercentage == null) {
									var n = noty({
										text : 'Error: RevenuePercentage must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}

								var logoPath = sessionStorage
										.getItem("fileData");

								var data =

								{
									'orgName' : $scope.orgName,
									'displayName' : $scope.displayName,
									'addressLine1' : $scope.addressLine1,
									'url' : $scope.url,
									'displayName' : $scope.displayName,
									'dateCreated' : $scope.dateCreated,
									'dateModified' : $scope.dateModified,
									'status' : $scope.status,
									'attributes' : $scope.attributes,
									'addressLine1' : $scope.addressLine1,
									'addressLine2' : $scope.addressLine2,
									'city' : $scope.city,
									'state' : $scope.state,
									'postalCode' : $scope.postalCode,
									'country' : $scope.country,
									'revenuePercentage' : $scope.revenuePercentage,
									'additionalNotes' : $scope.additionalNotes,
									'logoPath' : logoPath
								};

								console.log('Create Org ');
								console.log(data);

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/organization/createOrg",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Organization created successfully',
														layout : 'topRight',
														type : 'success'
													});
													//sessionStorage.clear(logoPath);
													window.location = redirectToOrgDetailURL
															+ $scope.orgName;

												})
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}

						} ]);



app
		.controller(
				'pollDashboardCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {
							console.log('Inside pollDashboardCtrl');

							var orgName = $routeParams.orgName;
							$scope.data = orgName;
							
							var logoPath= sessionStorage.getItem("fileData");
							console.log(logoPath);
							/*sessionStorage

							.removeItem("fileData");*/

							/*
							 * $scope.$apply(function() {
							 * document.alert(document
							 * .getElementById("logoPath").value);
							 * $scope.logoPath = document
							 * .getElementById("logoPath").value; });
							 */
							/*console.log("Inside restore func");

						    var newQuestion = sessionStorage.getItem(question);
						    console.log(newQuestion);
						    if (newQuestion !== null){
						    	console.log("Inside if cond");	
						    document.getElementById("question").value=newQuestion;
						    }*/
							
							
							
							
							$http
							.get(
									serverURL
											+ boomerangServer+"/api/server/organization/getOrg/"
											+ $routeParams.orgName+"?withLogo=true",
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {

										$scope.currentOrg = data;
										console.log(data);
										
										// $scope.currentEvents =
										// data;

									})
							.error(
									function(data, status, headers,
											config) {
										if(status == 401){
											checkStatus();
										}
										else{

										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});
										}
									});

							
							/*$scope.checkingEventName = function(){
								
								var eventName=$routeParams.eventName;
								if (eventName.indexOf('?') !== -1){
									  // alert("The string 'Hello World' contains the substring 'orl'!");
									    var correctedEventName= eventName.replace("?","%3F");
										 console.log(correctedEventName);
									  $scope.evntDet(correctedEventName);
									    
									}
									else{
									   // alert("The string 'Hello World' does not contain the substring 'orl'!");
										var correctedEventName =  eventName;
										$scope.evntDet(correctedEventName);
									}
								
								
								}*/
							
							
							
							
							
							$http
							.get(
									serverURL
											+ boomerangServer+"/api/server/eventservice/getEventUsingEventId/eventId/"
											+ $routeParams.eventId
											+ "/orgName/"
											+ $routeParams.orgName,
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {

										$scope.event = data;
										console.log(data);

									})
							.error(
									function(data, status, headers,
											config) {
										if(status == 401){
											checkStatus();
										}
										else{

										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											killer : 'true',
											type : 'error'
										});
										}
									});


							
							
							

							$scope.Createpoll = function() {
								
								console.log("Inside Createpoll func ");
								
								
							
							

								
								if ($scope.question == null) {
									var n = noty({
										text : 'Error: Poll Question must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								var logoPath = sessionStorage
								.getItem("fileData");
						
						console.log(logoPath);
								


								var optionsEntered = 0;

								var options = new Array(20);
								var options_json = "[";

								for (var i = 1; i < 20; i++) {
									var option = document
											.getElementById("field" + i);
										
									if (option != null) {
										if(option.value){
										optionsEntered = 1;
										options[i - 1] = option.value;
										}
									}
								}
								
							

								if (optionsEntered== 0) {
									var n = noty({
										text : 'Error: Option must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}

								
								var selectedAdvIds = new Array();

								var inputs = document
										.getElementsByTagName("input");

								for (input in inputs) {
									if (inputs[input].name == "advId"
											&& inputs[input].checked) {
										selectedAdvIds
												.push(inputs[input].value);
									}
								}

								console.log("selected advertiser Ids");
								console.log(selectedAdvIds);
								
								if($scope.socialMediaText!=null){
								var socialMedTxt=$scope.socialMediaText;
								var predefTxt= " with #BoomerangCast";
								var res;
								if(socialMedTxt){
								res = socialMedTxt.concat(predefTxt);
								}
								console.log(res);
								}
								
								
								
								
								if(logoPath==null){
								var isOrgLogoChecked=0;
								if(document.getElementById("isOrgLogoChecked").checked == true){
								isOrgLogoChecked=1;
								console.log(isOrgLogoChecked);
								}
								}
								
								if (logoPath==null) {
									
									if(isOrgLogoChecked==0){
									var n = noty({
										text : 'Error: Upload an image or use Organization logo for Poll Image',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
									
								}
								
								
								
								

								var data =

								{

									'pollId' : $scope.pollId,
									'eventId' : $scope.event.eventId,
									'orgId' : $routeParams.orgId,
									'question' : $scope.question,
									'socialMediaText' : res,
									'pollImage' : $scope.pollImage,
									'optionStrings' : options,
									'slotIds' : selectedAdvIds,
									'logoPath' : sessionStorage.getItem("fileData"),
									'isOrgLogoChecked' : isOrgLogoChecked

								};
								
								console.log(data);

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/eventservice/addPollToEvent",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Poll created successfully',
														layout : 'topRight',
														type : 'success'
													});
													sessionStorage

													.removeItem("fileData");
													
														  window.location = redirectToEventDetailsURL
															+ $routeParams.eventId
															+ "/orgName/"
															+ $routeParams.orgName;
												})
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}
							
							
							
							
							$scope.SaveAndCreatepoll = function() {								
								
								if ($scope.question == null) {
									var n = noty({
										text : 'Error: Poll Question must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								var logoPath = sessionStorage
								.getItem("fileData");
						console.log(logoPath);
						
						
						
								

								var optionsEntered = 0;

								var options = new Array(20);
								var options_json = "[";

								for (var i = 1; i < 20; i++) {
									var option = document
											.getElementById("field" + i);
										
									if (option != null) {
										if(option.value){
										optionsEntered = 1;
										options[i - 1] = option.value;
										}
									}
								}
								
							

								if (optionsEntered== 0) {
									var n = noty({
										text : 'Error: Option must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}

								
								var selectedAdvIds = new Array();

								var inputs = document
										.getElementsByTagName("input");

								for (input in inputs) {
									if (inputs[input].name == "advId"
											&& inputs[input].checked) {
										selectedAdvIds
												.push(inputs[input].value);
									}
								}

								console.log("selected advertiser Ids");
								console.log(selectedAdvIds);
								
								
							
								
								
								
								if($scope.socialMediaText!=null){
									var socialMedTxt=$scope.socialMediaText;
									var predefTxt= " with #BoomerangCast";
									var res=socialMedTxt.concat(predefTxt);
									console.log(res);
									}
								
								
								if(logoPath==null){
									var isOrgLogoChecked=0;
									if(document.getElementById("isOrgLogoChecked").checked == true){
									isOrgLogoChecked=1;
									console.log(isOrgLogoChecked);
									}
									}
									
									if (logoPath==null) {
										
										if(isOrgLogoChecked==0){
										var n = noty({
											text : 'Error: Upload a new image or use Organization logo for Poll Image',
											layout : 'topRight',
											killer : 'true',
											type : 'error'
										});
										return;
									}
										
									}

								var data =

								{

									'pollId' : $scope.pollId,
									'eventId' : $scope.event.eventId,
									'orgId' : $routeParams.orgId,
									'question' : $scope.question,
									'socialMediaText' : res,
									'pollImage' : $scope.pollImage,
									'optionStrings' : options,
									'slotIds' : selectedAdvIds,
									'isOrgLogoChecked' : isOrgLogoChecked,
									'logoPath' :  sessionStorage.getItem("fileData")

								};

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/eventservice/addPollToEvent",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Poll created successfully',
														layout : 'topRight',
														type : 'success'
													});
													sessionStorage
													.removeItem("fileData");

													
													window.location.reload();
												})
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}
							
							
							


							$http
									.get(
											serverURL
													+ boomerangServer+"/api/server/advertiserservice/listOfAdvertisersGroupByTransactionsUsingOrgId/orgId/"
													+ $routeParams.orgId,

											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												console.log(data);
												$scope.advLists = data;
												

												$scope.advIds = new Array();
												for (var i = 0; i < $scope.advLists.length; i++) {
													$scope.advIds[i] = $scope.advLists[i].advertiserId;
												}

												//console.log($scope.advIds);

												// $scope.currentEvents = data;

											})
									.error(
											function(data, status, headers,
													config) {

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													killer : 'true',
													type : 'error'
												});

											});
							


$scope.CreateUpcomingPoll = function() {								
	
	if ($scope.question == null) {
		var n = noty({
			text : 'Error: Poll Question must be entered',
			layout : 'topRight',
			killer : 'true',
			type : 'error'
		});
		return;
	}
	
	var logoPath = sessionStorage
	.getItem("fileData");
console.log(logoPath);



	var optionsEntered = 0;

	var options = new Array(20);
	var options_json = "[";

	for (var i = 1; i < 20; i++) {
		var option = document
				.getElementById("field" + i);
			
		if (option != null) {
			if(option.value){
			optionsEntered = 1;
			options[i - 1] = option.value;
			}
		}
	}
	


	if (optionsEntered== 0) {
		var n = noty({
			text : 'Error: Option must be entered',
			layout : 'topRight',
			killer : 'true',
			type : 'error'
		});
		return;
	}

	
	var selectedAdvIds = new Array();

	var inputs = document
			.getElementsByTagName("input");

	for (input in inputs) {
		if (inputs[input].name == "advId"
				&& inputs[input].checked) {
			selectedAdvIds
					.push(inputs[input].value);
		}
	}

	console.log("selected advertiser Ids");
	console.log(selectedAdvIds);
	
	if($scope.socialMediaText!=null){
	var socialMedTxt=$scope.socialMediaText;
	var predefTxt= " with #BoomerangCast";
	var res=socialMedTxt.concat(predefTxt);
	console.log(res);
	}
	if(logoPath==null){
		var isOrgLogoChecked=0;
		if(document.getElementById("isOrgLogoChecked").checked == true){
		isOrgLogoChecked=1;
		console.log(isOrgLogoChecked);
		}
		}
		
		if (logoPath==null) {
			
			if(isOrgLogoChecked==0){
			var n = noty({
				text : 'Error: Upload a new image or use Organization logo for Poll Image',
				layout : 'topRight',
				killer : 'true',
				type : 'error'
			});
			return;
		}
			
		}
	
	

	var data =

	{

		'pollId' : $scope.pollId,
		'eventId' : $scope.event.eventId,
		'orgId' : $routeParams.orgId,
		'question' : $scope.question,
		'socialMediaText' : res,
		'pollImage' : $scope.pollImage,
		'optionStrings' : options,
		'slotIds' : selectedAdvIds,
		'isOrgLogoChecked' : isOrgLogoChecked,
		'logoPath' :  sessionStorage.getItem("fileData")

	};
	
	console.log(data);

	$http
			.post(
					serverURL
							+ boomerangServer+"/api/server/eventservice/addPollToEvent",
					data,
					{
						headers : {
							'Content-Type' : 'application/json; charset=UTF-8'
						}
					})
			.success(
					function(data, status, headers,
							config) {

						var n = noty({
							text : 'Poll created successfully',
							layout : 'topRight',
							type : 'success'
						});
						sessionStorage
						.removeItem("fileData");

						window.location = redirectToUpcomingEventDetailsURL
						+ $routeParams.eventId
						+ "/orgName/"
						+ $routeParams.orgName;
					})
			.error(
					function(data, status, headers,
							config) {

						var n = noty({
							text : 'Error: '
									+ headers('BoomerangError'),
							layout : 'topRight',
							killer : 'true',
							type : 'error'
						});

					});

}

							
							
							
							

						} ]);

app
		.controller(
				'CreateeventDashboardCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {
							console.log('Inside CreateeventDashboardCtrl');
							
							$http
									.get(
											serverURL
													+ boomerangServer+"/api/server/organization/getOrgById/"
													+ $routeParams.orgId+"?fetchSummary=true",
											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												$scope.currentOrg = data;
												console.log(data);
												// $scope.currentEvents =
												// data;

											})
									.error(
											function(data, status, headers,
													config) {
												if(status == 401){
													checkStatus();
												}
												else{

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													type : 'error'
												});
												}

											});

							$scope.Createevent = function() {

								var d = new Date()
								var offset = d.getTimezoneOffset();
								if ($scope.eventName == null) {
									var n = noty({
										text : 'Error: Event Name must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.addressLine1 == null) {
									var n = noty({
										text : 'Error: Address Line 1 must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}

								if ($scope.latitude == null
										|| $scope.longitude == null) {
									var n = noty({
										text : 'Error: Need to Click "validate address" to fetch Latitude & Longitude',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.type == null) {
									var n = noty({
										text : 'Error: Type must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.broadcastRange == null) {
									var n = noty({
										text : 'Error: BroadcastRange must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								/*
								 * if ($scope.startDate == null) { var n =
								 * noty({ text : 'Error: Start Date must be
								 * entered', layout : 'topRight', killer :
								 * 'true', type : 'error' }); return; } if
								 * ($scope.endDate == null) { var n = noty({
								 * text : 'Error: End Date must be entered',
								 * layout : 'topRight', killer : 'true', type :
								 * 'error' }); return; }
								 */

								/*
								 * $scope.startDate = document
								 * .getElementById("startDt").value;
								 * console.log($scope.startDate);
								 */
								var startDateTime = new Date(Date.parse($scope.startDate)).toGMTString();
								var endDateTime = new Date(Date.parse($scope.endDate)).toGMTString();
								
								if (startDateTime == null) {
									var n = noty({
										text : 'Error: Start Date must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								if (endDateTime == null) {
									var n = noty({
										text : 'Error: End Date must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								
								
								var stMillis = new Date(Date.parse($scope.startDate)).getTime();
								var endMillis = new Date(Date.parse($scope.endDate)).getTime();
								
								
								/*if (startDateTime > endDateTime) {
									var n = noty({
										text : 'Error: End Date should not be previous to Start Date',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}*/
								
								
								
								

								var data =

								{

									'type' : $scope.type,
									'addressLine1' : $scope.addressLine1,
									'addressLine2' : $scope.addressLine2,
									'city' : $scope.city,
									'state' : $scope.state,
									'postalCode' : $scope.postalCode,
									'country' : $scope.country,
									'broadcastRange' : $scope.broadcastRange,
									'startTimeMillis' : stMillis,
									'endTimeMillis' : endMillis,
									'orgId' : $routeParams.orgId,
									'latitude' : $scope.latlng.latitude,
									'longitude' : $scope.latlng.longitude,
									'eventName' : $scope.eventName,
									'offset' : offset
								    //'orgName' : $scope.currentOrg.orgName,

								};

								console.log(data);
                                
								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/eventservice/createEvent",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Event created successfully',
														layout : 'topRight',
														type : 'success'
													});
												     
													
													window.location = redirectToOrgDashURL + $scope.currentOrg.orgId;
													
												})

										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: An Event with the same name is LIVE or scheduled to go LIVE during this time period',
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}
							
							
													
							
							
							$scope.createEventAndGotoCreatePoll = function() {

								if ($scope.eventName == null) {
									var n = noty({
										text : 'Error: Event Name must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.addressLine1 == null) {
									var n = noty({
										text : 'Error: Address Line 1 must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}

								if ($scope.latitude == null
										|| $scope.longitude == null) {
									var n = noty({
										text : 'Error: Need to Click "validate address" to fetch Latitude & Longitude',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.type == null) {
									var n = noty({
										text : 'Error: Type must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.broadcastRange == null) {
									var n = noty({
										text : 'Error: BroadcastRange must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								/*
								 * if ($scope.startDate == null) { var n =
								 * noty({ text : 'Error: Start Date must be
								 * entered', layout : 'topRight', killer :
								 * 'true', type : 'error' }); return; } if
								 * ($scope.endDate == null) { var n = noty({
								 * text : 'Error: End Date must be entered',
								 * layout : 'topRight', killer : 'true', type :
								 * 'error' }); return; }
								 */

								/*
								 * $scope.startDate = document
								 * .getElementById("startDt").value;
								 * console.log($scope.startDate);
								 */

								var startDateTime = $scope.startDate;
								var endDateTime = $scope.endDate;
								
								
								if (startDateTime == null) {
									var n = noty({
										text : 'Error: Start Date must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								if (endDateTime == null) {
									var n = noty({
										text : 'Error: End Date must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								var stMillis = new Date(Date.parse($scope.startDate)).getTime();
								var endMillis = new Date(Date.parse($scope.endDate)).getTime();
								
								/*if (startDateTime > endDateTime) {
									var n = noty({
										text : 'Error: End Date should not be previous to Start Date',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}*/

								var data =

								{

									'type' : $scope.type,
									'addressLine1' : $scope.addressLine1,
									'addressLine2' : $scope.addressLine2,
									'city' : $scope.city,
									'state' : $scope.state,
									'postalCode' : $scope.postalCode,
									'country' : $scope.country,
									'broadcastRange' : $scope.broadcastRange,
									'startTimeMillis' : stMillis,
									'endTimeMillis' : endMillis,
									'orgId' : $routeParams.orgId,
									'latitude' : $scope.latlng.latitude,
									'longitude' : $scope.latlng.longitude,
									'eventName' : $scope.eventName,
								// 'orgName' : $scope.currentOrg.orgName,

								};

								console.log(data);

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/eventservice/createEvent",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Event created successfully',
														layout : 'topRight',
														type : 'success'
													});
													var eventName = $scope.eventName;
													var res = encodeURIComponent(eventName);
													$scope.checkEventStatus(res);
													
													
													
													/*window.location = redirectToPollCreateURL + $scope.currentOrg.orgName
													+ "/orgId/" + $scope.currentOrg.orgId
															+ "/eventName/"
															+ $scope.eventName
															+ "/eventId/"
															+ $scope.eventId;*/
													// window.location =
													// redirectToEventDetailsURL+"eventName/"+$scope.event.eventName+"/eventId/"+$scope.event.eventId+"/orgName/"+$scope.event.orgName;

												})

										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: An Event with the same name is LIVE or scheduled to go LIVE during this time period',
																
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}
							
							$scope.checkEventStatus = function(res){
								console.log("inside checkEventStatus");
								console.log(res);
								
								
								$http
								.get(
										serverURL
												+ boomerangServer+"/api/server/eventservice/checkEventStatus/eventName/"
												+ res + "/orgName/" + $scope.currentOrg.orgName ,
										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {
											
											console.log(data);
											$scope.fetchedEvent = data;
											
											if($scope.fetchedEvent.status==1){
												window.location = redirectToCurrentPollCreateURL + $scope.currentOrg.orgName
												+ "/orgId/" + $scope.currentOrg.orgId + "/eventId/" + $scope.fetchedEvent.eventId;
												
											}
											
											if($scope.fetchedEvent.status==2){
												window.location = redirectToUpcomingPollCreateURL + $scope.currentOrg.orgName
												+ "/orgId/" + $scope.currentOrg.orgId + "/eventId/" + $scope.fetchedEvent.eventId;
												
											}
											
											if($scope.fetchedEvent.status==0){
												window.location = redirectToOrgDashURL + $scope.currentOrg.orgId;
											}
											
											
											

										})
								.error(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'Error: '
														+ headers('BoomerangError'),
												layout : 'topRight',
												type : 'error'
											});

										});
								
								
							}


							$scope.showInMap = function() {
								if ($scope.eventName == null) {
									var n = noty({
										text : 'Error: eventName must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.addressLine1 == null) {
									var n = noty({
										text : 'Error: addressLine1 must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}

								var data =

								{

									'type' : $scope.type,
									'addressLine1' : $scope.addressLine1,
									'addressLine2' : $scope.addressLine2,
									'city' : $scope.city,
									'state' : $scope.state,
									'postalCode' : $scope.postalCode,
									'country' : $scope.country,
									'broadcastRange' : $scope.broadcastRange,
									'startDate' : $scope.startDate,
									'endDate' : $scope.endDate,
									'orgId' : $routeParams.orgId,
									'latitude' : $scope.latitude,
									'longitude' : $scope.longitude,
									'eventName' : $scope.eventName,
								// 'orgName' : $scope.orgName,

								};

								console.log(data);

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/addressconverter/getLatLong",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													/*
													 * var n = noty({ text : '',
													 * layout : 'top', type :
													 * 'success' });
													 */
													$scope.latlng = data;
													$scope.latitude = $scope.latlng.latitude;
													$scope.longitude = $scope.latlng.longitude;

													document

															.getElementById('gmap').innerHTML = '<iframe style="border:none" id=map width="100%" height="100%" src="http://maps.google.com?q='

															+ $scope.latlng.latitude
															+ ','
															+ $scope.latlng.longitude
															+ '&output=embed" scrolling="yes"> ';

												})
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error:Unable to Fetch Map.Network is too slow or Unavailable',												
														layout : 'top',
														killer : 'true',
														type : 'error'
													});

												});

								

							}

						} ]);

app
		.controller(
				'createSponsorDashboardCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {
console.log('Inside createSponsorDashboardCtrl');
$http
.get(
		serverURL
				+ boomerangServer+"/api/server/organization/getOrgById/"
				+ $routeParams.orgId+"?fetchSummary=true",
		{
			headers : {
				'Content-Type' : 'application/json; charset=UTF-8'
			}
		})
.success(
		function(data, status, headers,
				config) {

			$scope.currentOrg = data;
			console.log(data);
			// $scope.currentEvents =
			// data;

		})
.error(
		function(data, status, headers,
				config) {
			if(status == 401){
				checkStatus();
			}
			else{
			

			var n = noty({
				text : 'Error: '
						+ headers('BoomerangError'),
				layout : 'topRight',
				type : 'error'
			});
			}

		});

							$scope.CreateSponsor = function() {
								if ($scope.businessName == null) {
									var n = noty({
										text : 'Error: Business Name must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if($scope.typeOfBusiness == null){
									var n = noty({
										text : 'Error: Type Of Business must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
											
								}
								if ($scope.contactFname == null) {
									var n = noty({
										text : 'Error: Contact Person First Name must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.contactEmail == null) {
									var n = noty({
										text : 'Error:  Correct Email must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if($scope.addressLine1 == null){
									var n = noty({
										text : 'Error: AddressLine1 must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
									
								}
								if($scope.city == null){
									var n = noty({
										text : 'Error: City must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
									
								}
								if($scope.state == null){
									var n = noty({
										text : 'Error: State must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
									
								}
								if($scope.postalCode == null){
									var n = noty({
										text : 'Error: Zip must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
									
								}
								
								/*if ($scope.contactPhoneNumber == null) {
									var n = noty({

										text : 'Error:contactPhoneNumber must be entered',

										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}*/
								
								

								var logoPath = sessionStorage
										.getItem("fileData");
								
								if (logoPath == null) {
									var n = noty({
										text : 'Error: Choose Advertising Image',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if($scope.adRedirectUrl == null){
									var n = noty({
										text : 'Error:  Website URL must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
									
								}
								
								/*if ($scope.adRedirectUrl == null) {
									var n = noty({
										text : 'Error: URL must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}*/
								var adRedirectUrl=$scope.adRedirectUrl;
								var predefTxt= "http://";
								var redirectUrl=predefTxt.concat(adRedirectUrl);
								console.log(redirectUrl);

								var data =

								{

									'advertiserId' : $scope.advertiserId,
									'businessName' : $scope.businessName,
									'contactFname' : $scope.contactFname,
									'contactLname' : $scope.contactLname,
									'contactEmail' : $scope.contactEmail,
									'contactPhoneAreacode' : $scope.contactPhoneAreacode,
									'contactPhoneNumber' : $scope.contactPhoneNumber,
									'addressLine1' : $scope.addressLine1,
									'addressLine2' : $scope.addressLine2,
									'city' : $scope.city,
									'state' : $scope.state,
									'postalCode' : $scope.postalCode,
									'country' : $scope.country,
									'typeOfBusiness' : $scope.typeOfBusiness,
									'additionalNotes' : $scope.additionalNotes,
									'orgId' : $routeParams.orgId,
									'orgName' : $routeParams.orgName,
									'logoPath' : sessionStorage.getItem("fileData"),
									'adRedirectUrl' : redirectUrl,
									'notes' : $scope.notes,
									'label' : $scope.label

								};
								console.log(data);
								
								

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/advertiserservice/createAdvertiser",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Advertiser created successfully',
														layout : 'topRight',
														type : 'success'
													});
													
													sessionStorage.removeItem("fileData");
													var businessName = $scope.businessName;
													var res = encodeURIComponent(businessName);
													$scope.FetchAdvertiserId(res);
												})
										.error(
												function(data, status, headers,
														config) {
													

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});
													

												});

							}
							
							$scope.FetchAdvertiserId = function(res){
								console.log("Inside FetchAdvertiserId ");
								console.log(res);
								var URL = serverURL
								+ boomerangServer+"/api/server/advertiserservice/AdvertiserUsingAdvertiserNameAndOrgName/advertiserName/"
								+ res + "/orgName/"
								+ $scope.currentOrg.orgName;

						

						$http
								.get(
										URL,

										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {
											console.log(data);

											$scope.adr = data;
											var advertiserId=$scope.adr.advertiserId;
											window.location = redirectToSponsorDetailsURL
											+ advertiserId
											+ "/orgName/"
											+ $scope.currentOrg.orgName;
										})
								.error(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'Error: '
														+ headers('BoomerangError'),
												layout : 'topRight',
												killer : 'true',
												type : 'error'
											});

										});
								
								
								
								
							}
							
							$scope.SaveAndSellAd = function() {
								console.log('Inside SaveAndSellAd');
								if ($scope.businessName == null) {
									var n = noty({
										text : 'Error: Business Name must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.contactFname == null) {
									var n = noty({
										text : 'Error: Contact Person First Name must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if ($scope.contactEmail == null) {
									var n = noty({
										text : 'Error: Email must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								/*if ($scope.contactPhoneNumber == null) {
									var n = noty({

										text : 'Error:contactPhoneNumber must be entered',

										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}*/
								
								

								var logoPath = sessionStorage
								.getItem("fileData");
								
								if (logoPath == null) {
									var n = noty({
										text : 'Error: Choose Advertising Image',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								/*if ($scope.adRedirectUrl == null) {
									var n = noty({
										text : 'Error: URL must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}*/
								var adRedirectUrl=$scope.adRedirectUrl;
								var predefTxt= "http://";
								var redirectUrl=predefTxt.concat(adRedirectUrl);
								console.log(redirectUrl);

								var data =

								{

									'advertiserId' : $scope.advertiserId,
									'businessName' : $scope.businessName,
									'contactFname' : $scope.contactFname,
									'contactLname' : $scope.contactLname,
									'contactEmail' : $scope.contactEmail,
									'contactPhoneAreacode' : $scope.contactPhoneAreacode,
									'contactPhoneNumber' : $scope.contactPhoneNumber,
									'addressLine1' : $scope.addressLine1,
									'addressLine2' : $scope.addressLine2,
									'city' : $scope.city,
									'state' : $scope.state,
									'postalCode' : $scope.postalCode,
									'country' : $scope.country,
									'typeOfBusiness' : $scope.typeOfBusiness,
									'additionalNotes' : $scope.additionalNotes,
									'orgId' : $routeParams.orgId,
									'orgName' : $routeParams.orgName,
									'logoPath' :  sessionStorage.getItem("fileData"),
									'adRedirectUrl' : redirectUrl,
									'notes' : $scope.notes,
									'label' : $scope.label

								};
								
								

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/advertiserservice/createAdvertiser",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Advertiser created successfully',
														layout : 'topRight',
														type : 'success'
													});
													
													sessionStorage
													.removeItem("fileData");
													var businessName = $scope.businessName;
													var res = encodeURIComponent(businessName);
													$scope.GetCurrentSponsor(res);
												})
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}
							
							
							$scope.GetCurrentSponsor=function(res){
								$http
								.get(
										serverURL
										+ boomerangServer+"/api/server/advertiserservice/AdvertiserUsingAdvertiserNameAndOrgName/advertiserName/"
										+ res + "/orgName/"
										+ $scope.currentOrg.orgName,

										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {

											$scope.adr = data;
											var advertiserId=$scope.adr.advertiserId;
											window.location= redirectToSellAdURL + $scope.currentOrg.orgName
											+ "/orgId/" + $scope.adr.orgId + "/advId/" + advertiserId;
										})
								.error(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'Error: '
														+ headers('BoomerangError'),
												layout : 'topRight',
												killer : 'true',
												type : 'error'
											});

										});

								
								
								
								
								
								
							}

							

						} ]);

app
		.controller(
				'CurrentEventDetailsCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {
							console.log('Inside CurrentEventDetailsCtrl');
							
							
							$http
							.get(
									serverURL
											+ boomerangServer+"/api/server/organization/getOrg/"
											+ $routeParams.orgName+"?withLogo=true",
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {

										$scope.currentOrg = data;
										console.log(data);
										

									})
							.error(
									function(data, status, headers,
											config) {
										if(status == 401){
											checkStatus();
										}
										else{

										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});
										}

									});


							
							$http
									.get(
											serverURL
													+ boomerangServer+"/api/server/eventservice/getEventUsingEventId/eventId/"
													+ $routeParams.eventId
													+ "/orgName/"
													+ $routeParams.orgName,
											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												$scope.event = data;
												var eventId=$scope.event.eventId;
												console.log(eventId);
												
												$scope.getListofPollWithOptions(eventId);
												$scope.preLoadMap();

											})
									.error(
											function(data, status, headers,
													config) {

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													killer : 'true',
													type : 'error'
												});

											});

							
							
							

							$scope.preLoadMap = function(){
							var data =

							{

								'type' : $scope.event.type,
								'addressLine1' : $scope.event.addressLine1,
								'addressLine2' : $scope.event.addressLine2,
								'city' : $scope.event.city,
								'state' : $scope.event.state,
								'postalCode' : $scope.event.postalCode,
								'country' : $scope.event.country,
								'broadcastRange' : $scope.event.broadcastRange,
								'startDate' : $scope.event.startDate,
								'endDate' : $scope.event.endDate,
								'orgId' : $routeParams.orgId,
								'latitude' : $scope.event.latitude,
								'longitude' : $scope.event.longitude,
								'eventName' : $scope.event.eventName,
								'orgName' : $scope.event.orgName

							};

							console.log(data);

							$http
									.post(
											serverURL
													+ boomerangServer+"/api/server/addressconverter/getLatLong",
											data,
											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												$scope.latlng = data;
												$scope.latitude = $scope.latlng.latitude;
												$scope.longitude = $scope.latlng.longitude;

												document
														.getElementById('gmap').innerHTML = '<iframe id=map height="100%" width="100%" src="http://maps.google.com?q='
														+ $scope.latlng.latitude
														+ ','
														+ $scope.latlng.longitude
														+ '&output=embed" scrolling="yes"> ';

											})
									.error(
											function(data, status, headers,
													config) {

												var n = noty({
													text : 'Error:Unable to Fetch Map.Network is too slow or Unavailable',															
													layout : 'top',
													killer : 'true',
													type : 'error'
												});

											});

							}
							
							$scope.getListofPollWithOptions = function(eventId){
								console.log("getListofPollWithOptions");
								console.log(eventId);
								
								$http
								.get(
										
										serverURL
												+ boomerangServer+"/api/server/eventservice/PollsForEvent/eventId/"
												+ eventId + "?includeResult=true",

										{
											headers : {
												'Content-Type' : 'application/json; charset=UTF-8'
											}
										})
								.success(
										function(data, status, headers,
												config) {

											$scope.polls = data;
											console.log(data);
											

										})
								.error(
										function(data, status, headers,
												config) {

											var n = noty({
												text : 'Error: '
														+ headers('BoomerangError'),
												layout : 'topRight',
												killer : 'true',
												type : 'error'
											});

										});

								
							}
							
							
							
							
							
							$scope.updateEvent = function() {

								console.log('Inside updateEvent function');
								var startDateTime =$scope.event.startDate;
								var endDateTime = $scope.event.endDate;

								if ($scope.latitude == null
										&& $scope.longitude == null) {
									var n = noty({
										text : 'Error: Need to Click "validate address" to fetch Latitude & Longitude',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								if (startDateTime == null) {
									var n = noty({
										text : 'Error: Start Date must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								if (endDateTime == null) {
									var n = noty({
										text : 'Error: End Date must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								
								var stMillis = new Date(Date.parse($scope.event.startDate)).getTime();
								var endMillis = new Date(Date.parse($scope.event.endDate)).getTime();
								
								
								/*if (startDateTime > endDateTime) {
									var n = noty({
										text : 'Error: End Date should not be previous to Start Date',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
*/
								var data =

								{

									'eventId' : $scope.event.eventId,
									'type' : $scope.event.type,
									'addressLine1' : $scope.event.addressLine1,
									'addressLine2' : $scope.event.addressLine2,
									'city' : $scope.event.city,
									'state' : $scope.event.state,
									'postalCode' : $scope.event.postalCode,
									'country' : $scope.event.country,
									'broadcastRange' : $scope.event.broadcastRange,
									'startTimeMillis' : stMillis,
									'endTimeMillis' : endMillis,
									'latitude' : $scope.latitude,
									'longitude' : $scope.longitude,
									'eventName' : $scope.event.eventName,
									'orgId' : $scope.currentOrg.orgId
									

								};

								console.log(data);
								

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/eventservice/updateEvent",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Event Updated  successfully',
														layout : 'topRight',
														type : 'success'
													});
													
													
													
														  window.location = redirectToUpcomingEventDetailsURL
															+ $routeParams.eventId
															+ "/orgName/"
															+ $routeParams.orgName;
													    
													
												})

										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: An Event with the same name is LIVE or scheduled to go LIVE during this time period',
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}

							endEvent = function() {
								console.log('Inside endEvent function');

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/eventservice/endEventByEventId/eventId/"
														+ $scope.event.eventId
														+ "/orgName/"
														+ $scope.event.orgName,

												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Event Ended Successfully',
														layout : 'topRight',
														type : 'success'
													});

													
													window.location = redirectToOrgDashURL
															+ $scope.event.orgId;
													window.location.reload();

												})

										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}

							$scope.showInMap = function() {

								var data =

								{

									'type' : $scope.event.type,
									'addressLine1' : $scope.event.addressLine1,
									'addressLine2' : $scope.event.addressLine2,
									'city' : $scope.event.city,
									'state' : $scope.event.state,
									'postalCode' : $scope.event.postalCode,
									'country' : $scope.event.country,
									'broadcastRange' : $scope.event.broadcastRange,
									'startDate' : $scope.event.startDate,
									'endDate' : $scope.event.endDate,
									'orgId' : $routeParams.orgId,
									'latitude' : $scope.latitude,
									'longitude' : $scope.longitude,
									'eventName' : $scope.event.eventName,
									'orgName' : $scope.event.orgName,

								};

								console.log(data);

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/addressconverter/getLatLong",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													$scope.latlng = data;
													$scope.latitude = $scope.latlng.latitude;
													$scope.longitude = $scope.latlng.longitude;

													document
															.getElementById('gmap').innerHTML = '<iframe id=map  width="100%" height="100%" src="http://maps.google.com?q='
															+ $scope.latlng.latitude
															+ ','
															+ $scope.latlng.longitude
															+ '&output=embed" scrolling="yes"> ';

												})
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error:Unable to Fetch Map.Network is too slow or Unavailable',																
														layout : 'top',
														killer : 'true',
														type : 'error'
													});

												});

							}
							

							

						} ]);

app
		.controller(
				'PreviousEventDetailsCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {
							console.log('Inside PreviousEventDetailsCtrl');
							
							$http
							.get(
									serverURL
											+ boomerangServer+"/api/server/organization/getOrg/"
											+ $routeParams.orgName+"?withLogo=true",
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {

										$scope.currentOrg = data;
										console.log(data);
										
										// $scope.currentEvents =
										// data;

									})
							.error(
									function(data, status, headers,
											config) {
										if(status == 401){
											checkStatus();
										}
										else{

										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});
										}

									});

							
							
							
							
							
							
							$http
									.get(
											serverURL
													+ boomerangServer+"/api/server/eventservice/getEventUsingEventId/eventId/"
													+ $routeParams.eventId
													+ "/orgName/"
													+ $routeParams.orgName,
											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												$scope.event = data;
												$scope.preLoadMap();
												var eventId=$scope.event.eventId;
												$scope.getListofPollWithOptions(eventId);
												$scope.getListOfRedeemedAdvertisers(eventId);

											})
									.error(
											function(data, status, headers,
													config) {

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													killer : 'true',
													type : 'error'
												});

											});
							
						
							
							$scope.preLoadMap = function(){
								var data =

								{

									'type' : $scope.event.type,
									'addressLine1' : $scope.event.addressLine1,
									'addressLine2' : $scope.event.addressLine2,
									'city' : $scope.event.city,
									'state' : $scope.event.state,
									'postalCode' : $scope.event.postalCode,
									'country' : $scope.event.country,
									'broadcastRange' : $scope.event.broadcastRange,
									'startDate' : $scope.event.startDate,
									'endDate' : $scope.event.endDate,
									'orgId' : $routeParams.orgId,
									'latitude' : $scope.event.latitude,
									'longitude' : $scope.event.longitude,
									'eventName' : $scope.event.eventName,
									'orgName' : $scope.event.orgName

								};

								console.log(data);

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/addressconverter/getLatLong",
												data,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													$scope.latlng = data;
													$scope.latitude = $scope.latlng.latitude;
													$scope.longitude = $scope.latlng.longitude;

													document
															.getElementById('gmap').innerHTML = '<iframe id=map height="100%" width="100%" src="http://maps.google.com?q='
															+ $scope.latlng.latitude
															+ ','
															+ $scope.latlng.longitude
															+ '&output=embed" scrolling="yes"> ';

												})
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error:Unable to Fetch Map.Network is too slow or Unavailable ',																
														layout : 'top',
														killer : 'true',
														type : 'error'
													});

												});

								}
								
								
																
								
								$scope.getListofPollWithOptions = function(eventId){
									
									
									$http
									.get(
											
											serverURL
													+ boomerangServer+"/api/server/eventservice/PollsForEvent/eventId/"
													+ eventId + "?includeResult=true",

											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												$scope.polls = data;
												console.log(data);
												

											})
									.error(
											function(data, status, headers,
													config) {

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													killer : 'true',
													type : 'error'
												});

											});

									
								}
								
								
								
								$scope.getListOfRedeemedAdvertisers = function(eventId){
									
									$http
									.get(
											
											serverURL
													+ boomerangServer+"/api/server/advertiserservice/listOfRedeemedAdvertisersForEvent/eventId/"
													+ eventId,

											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												$scope.redeemedList = data;
												console.log(data);

											})
									.error(
											function(data, status, headers,
													config) {

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													killer : 'true',
													type : 'error'
												});

											});

								}
								
								


						}

				]);






app
		.controller(
				'AdminDetailsCtrl',
				[
						'$scope',
						'$http',
						'$routeParams',
						function($scope, $http, $routeParams) {
							console.log('Inside AdminDetailsCtrl');
							
							$http
							.get(
									serverURL
											+ boomerangServer+"/api/server/organization/getOrg/"
											+ $routeParams.orgName+"?withLogo=true",
									{
										headers : {
											'Content-Type' : 'application/json; charset=UTF-8'
										}
									})
							.success(
									function(data, status, headers,
											config) {

										$scope.currentOrg = data;
										console.log(data);
										// $scope.currentEvents =
										// data;

									})
							.error(
									function(data, status, headers,
											config) {
										if(status == 401){
											checkStatus();
										}
										else{
										var n = noty({
											text : 'Error: '
													+ headers('BoomerangError'),
											layout : 'topRight',
											type : 'error'
										});
										}

									});

							$http
									.get(
											serverURL
													+ boomerangServer+"/api/server/adminmanagerservice/getAdmin/userName/"
													+ $routeParams.username,

											{
												headers : {
													'Content-Type' : 'application/json; charset=UTF-8'
												}
											})
									.success(
											function(data, status, headers,
													config) {

												$scope.admin = data;
												$scope.admin.orgName = $routeParams.orgName;
												console
														.log($scope.admin.orgName);

											})
									.error(
											function(data, status, headers,
													config) {

												var n = noty({
													text : 'Error: '
															+ headers('BoomerangError'),
													layout : 'topRight',
													killer : 'true',
													type : 'error'
												});

											});

							$scope.updateAdmin = function() {

								if ($scope.admin.email == null) {
									var n = noty({
										text : 'Error: Email must be entered',
										layout : 'topRight',
										killer : 'true',
										type : 'error'
									});
									return;
								}
								console.log('Inside updateAdmin function');
								var data =

								{

									'adminId' : $scope.admin.adminId,
									'username' : $scope.admin.email,
									'fname' : $scope.admin.fname,
									'lname' : $scope.admin.lname,
									'email' : $scope.admin.email,
									'title' : $scope.admin.title,
									'orgId' : $scope.admin.orgId,
									'orgName' : $scope.admin.orgName,
									'status' : $scope.admin.status,
									'password' : $scope.admin.password

								};

								console.log(data);

								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/adminmanagerservice/updateAdmin",
												data,

												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {
													console
															.log($scope.admin.orgName);
													var n = noty({
														text : 'Administrator updated successfully',
														layout : 'topRight',
														type : 'success'
													});

													// window.location =
													// redirectToOrgDashURL+$scope.orgId;
													window.location = redirectToAdminDetailURL
															+ "username/"
															+ $scope.admin.email
															+ "/orgName/"
															+ $scope.admin.orgName;
													// window.location.reload();

												})
										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														killer : 'true',
														type : 'error'
													});

												});

							}

							$scope.deleteadmin = function(username) {
								console.log(username);
								$http
										.post(
												serverURL
														+ boomerangServer+"/api/server/adminmanagerservice/deleteAdmin/username/"
														+ username,
												{
													headers : {
														'Content-Type' : 'application/json; charset=UTF-8'
													}
												})
										.success(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Admin  deleted successfully',
														layout : 'topRight',
														type : 'success'
													});
													window.location = redirectToOrgDashURL
															+ $scope.admin.orgId;

													// window.location.reload();

												})

										.error(
												function(data, status, headers,
														config) {

													var n = noty({
														text : 'Error: '
																+ headers('BoomerangError'),
														layout : 'topRight',
														type : 'error'
													});

												});

							}

						} ]);

/** *** End Event Modal Controller **** */

/*app.controller('EndEventModalDemoCtrl', function($scope, $modal, $log) {

	$scope.eventName = "";

	$scope.open = function(size, evName) {
		$rootScope.eventName = evName;

		var modalInstance = $modal.open({
			templateUrl : 'angularjs.html',
			controller : 'EndEventModalInstanceCtrl',
			size : size,
			resolve : {
				eventName : function() {
					return $scope.eventName;
				}
			}
		});

		modalInstance.result.then(function(eventName) {

		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
});

app.controller('EndEventModalInstanceCtrl', function($scope, $modalInstance) {

	$scope.ok = function() {
		window.alert('Event ended ' + $rootScope.eventName);

		$modalInstance.close($scope.fileName);
	};

	$scope.endEventByObject = function() {
		window.alert('Event By Object ended ' + $rootScope.eventName);

		$modalInstance.close($rootScope.eventName);
	};

	$scope.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
});*/

/** ***** End Event Modal controller ***** */


function addFile(){
	console.log('Here');
	  var f = document.getElementById('file').files[0],
	      r = new FileReader();
	  r.onloadend = function(e){
	    var data = e.target.result;
	    sessionStorage.setItem("fileData",e.target.result);
	   
	  }
	  r.readAsDataURL(f);
	}
