/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';


    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload({
    		url: '/upload',
            done: function (e, data) {
                $.each(data.result, function (index, file) {
                	window.alert(file.name);
                    $('<p/>').text(file.name).appendTo(document.body);
                });
            }
    });

    

});
